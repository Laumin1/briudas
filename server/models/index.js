module.exports = {
    Guild: require('./guildModel'),
    Member: require('./memberModel'),
    Channel: require('./channelModel')
};